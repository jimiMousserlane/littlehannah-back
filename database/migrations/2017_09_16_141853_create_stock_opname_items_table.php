<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockOpnameItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_opname_items', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('stockOpnameId')->unsigned();
            $table->foreign('stockOpnameId')->references('id')
            ->on('stock_opnames')->onDelete('cascade');

            $table->integer('productDetailsId')->unsigned();
            $table->foreign('productDetailsId')->references('id')
            ->on('product_details')->onDelete('cascade');

            $table->integer('realQty');
            $table->integer('SOQty');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_opname_items');
    }
}
