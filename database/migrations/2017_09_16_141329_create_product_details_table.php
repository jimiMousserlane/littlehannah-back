<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_details', function (Blueprint $table) {
            $table->increments('id');

            $table->float('length')->default(0);
            $table->float('height')->default(0);
            $table->float('weight')->default(0);

            $table->boolean('showLength')->default(false);
            $table->boolean('showHeight')->default(false);
            $table->boolean('showWeight')->default(false);

            $table->integer('lastStockQty');
            $table->integer('minimumQty');

            $table->boolean('isActive');

            $table->integer('productId')->unsigned();
            $table->foreign('productId')->references('id')->on('products')->onDelete('cascade');

            $table->integer('productColorId')->unsigned();
            $table->foreign('productColorId')->references('id')->on('product_colors')->onDelete('cascade');

            $table->integer('productSizeId')->unsigned();
            $table->foreign('productSizeId')->references('id')->on('product_sizes')->onDelete('cascade');

            $table->string('UID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_details');
    }
}
