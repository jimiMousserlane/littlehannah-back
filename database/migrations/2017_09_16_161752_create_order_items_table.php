<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('qty');
            $table->decimal('price');
            $table->decimal('discount');
            $table->boolean('deleted');

            $table->integer('orderId')->unsigned();
            $table->foreign('orderId')->references('id')
            ->on('orders')->onDelete('cascade');

            $table->integer('productDetailsId')->unsigned();
            $table->foreign('productDetailsId')->references('id')
            ->on('product_details');

            $table->integer('promotionItemId')->unsigned()->nullable();
            $table->foreign('promotionItemId')->references('id')
            ->on('promotion_items');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
    }
}
