<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->decimal('price');
            $table->string('description');
            $table->string('notes');
            $table->string('productTags');
            $table->string('defaultImage');
            $table->boolean('isActive')->default(true);
            $table->decimal('tenantMargin');

            $table->integer('tenantId')->unsigned()->nullable();
            $table->foreign('tenantId')->references('id')->on('tenants')->onDelete('cascade');

            $table->string('UID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
