<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductJournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_journals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->datetime('journalDate');
            $table->string('journalType');
            $table->boolean('approved');
            $table->boolean('isAddQty');
            $table->string('UID');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_journals');
    }
}
