<?php

use Illuminate\Database\Seeder;
use Webpatser\Uuid\Uuid;

class DefaultData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i <= 3; $i++) { 
            DB::table('tenants')->insert([
                'name' => 'Tenant ' . $i,
                'address' => 'Address Tenant ' . $i,
                'logo' => '/img/tenants/tenant_' . $i . '/logo.jpg',
                'sizeGuide' => '/img/tenants/tenant_' . $i . '/sizeGuide.png',
                'description' => 'Description Tenant ' . $i,
                'UID' => Uuid::generate()
            ]);
        }

        for ($i=1; $i <= 8; $i++) { 
            DB::table('products')->insert([
                'name' => 'Product ' . $i,
                'price' => (rand(100, 200) * 1000),
                'description' => 'This is Product ' . $i,
                'notes' => '-',
                'productTags' => '#myproduct #newarrival',
                'defaultImage' => '/img/products/product_' . $i . '/p' . $i . '.jpg',
                'isActive' => true,
                'tenantMargin' => 25000,
                'tenantId' => rand(1,3),
                'UID' => Uuid::generate()
            ]);
        }


    }
}
