<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_infos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('orderId')->unsigned();
            $table->foreign('orderId')->references('id')
            ->on('orders')->onDelete('cascade');

            $table->string('customerName');
            $table->string('phoneNumber');
            $table->string('email');
            $table->string('address');
            $table->string('country');
            $table->string('regionstate');
            $table->string('city');
            $table->string('zipCode');
            $table->string('notes');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_infos');
    }
}
