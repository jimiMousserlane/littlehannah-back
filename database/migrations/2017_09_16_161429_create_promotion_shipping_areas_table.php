<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionShippingAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotion_shipping_areas', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('promotionId')->unsigned();
            $table->foreign('promotionId')->references('id')
            ->on('promotions')->onDelete('cascade');

            $table->string('area');

            $table->decimal('nominal');
            $table->decimal('percentage');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotion_shipping_areas');
    }
}
